package simulador;

public class binario {
    
    public String binario(int numero) {
		int d = numero;
		StringBuffer binario = new StringBuffer(); // guarda os dados
		while (d > 0) {
			int b = d % 2;
			binario.append(b);
			d = d >> 1; // é a divisão que você deseja
		}
		return binario.reverse().toString(); // inverte a ordem e imprime
	}
}