package simulador;

import javax.swing.table.DefaultTableModel;

public class controle {
    
    /*
    *
    *DECLARAÇÃO DE VARIAVEIS 
    *
    */
    
    private int pc,rsd,rtd,rdd,deslcd,shamt,resultula;
    boolean vazio;
    private int regs[];
    String rdest,r1,r2,deslc,ins;
    private String instrucao,funct;
    public DefaultTableModel model1,dados,model2;
   

    public controle(DefaultTableModel m, int pc,int[] vet,DefaultTableModel dados,DefaultTableModel model) {
        
        // RECEBENDO VALORES DA RAM , PC , VETOR COM OS REGISTRADORES
        
        this.model1 = m;
        this.pc = pc;
        this.regs = vet;
        this.dados = dados;
        this.model2=model;
    }
    
    /*
    *
    *NESSA FUNÇÃO NOS BUSCAMOS A INSTRUÇÃO E REALIZANDO PRIMEIRA PARTE DO CICLO 
    *
    */
    
    public void busca_instrucao(){ 
        instrucao = (String)model1.getValueAt(pc,3);
        if(instrucao.equals("")){
            vazio=true;
        }
    }
    
    /*
    *
    *NESSA FUNÇÃO NOS DIVIDIMOS A INSTRUÇÃO EM SEUS RESPECTIVOS REGISTRADORES 
    *E REALIZANDO A SEGUNDA PARTE DO CICLO
    *
    */
    
    public int leituradeoperando(){
        
        if(vazio==false){
            switch(instrucao.substring(0,6)) {
                case "000000": // INSTRUÇÃO DO TIPO R
                    rdest=instrucao.substring(6,11);
                    r1=instrucao.substring(11,16);
                    r2=instrucao.substring(16,21);
                    rdd = Integer.parseInt(rdest,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    rsd = Integer.parseInt(r1,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    rtd = Integer.parseInt(r2,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    funct = instrucao.substring(26,32);
                    shamt = Integer.parseInt(instrucao.substring(21,26));
                    break;
                case "100011": //INSTRUÇÃO DE TIPO LW
                    r1=instrucao.substring(6,11);
                    r2=instrucao.substring(11,16);
                    deslc=instrucao.substring(16,32);
                    rsd = Integer.parseInt(r1,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    rtd = Integer.parseInt(r2,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    deslcd = Integer.parseInt(deslc,2); //CONVERTANDO DE BINARIO PARA INTEIRO            
                break;
                case "101011": //INSTRUÇÃO DE TIPO SW
                    r1=instrucao.substring(6,11);
                    r2=instrucao.substring(11,16);
                    deslc=instrucao.substring(16,32);
                    rsd = Integer.parseInt(r1,2); //CONVERTANDO DE BINARIO PARA INTEIRO;
                    rtd = Integer.parseInt(r2,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    deslcd = Integer.parseInt(deslc,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                break;
                case "000100": //INSTRUÇÃO DE TIPO BEQ
                    r1=instrucao.substring(6,11);
                    r2=instrucao.substring(11,16);
                    deslc=instrucao.substring(16,32);
                    rsd = Integer.parseInt(r1,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    rtd = Integer.parseInt(r2,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    deslcd = Integer.parseInt(deslc,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                break;
                case "000101": //INSTRUÇÃO DE TIPO BNE
                    r1=instrucao.substring(6,11);
                    r2=instrucao.substring(11,16);
                    deslc=instrucao.substring(16,32);
                    rsd = Integer.parseInt(r1,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    rtd = Integer.parseInt(r2,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    deslcd = Integer.parseInt(deslc,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                break;
                case "001000": //INSTRUÇÃO DE TIPO ADDI 
                    r1=instrucao.substring(6,11);
                    r2=instrucao.substring(11,16);
                    deslc=instrucao.substring(16,32);
                    rsd = Integer.parseInt(r1,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    rtd = Integer.parseInt(r2,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    deslcd = Integer.parseInt(deslc,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                break;
                case "001101": //INSTRUÇÃO DE TIPO ORI
                    r1=instrucao.substring(6,11);
                    r2=instrucao.substring(11,16);
                    deslc=instrucao.substring(16,32);
                    rsd = Integer.parseInt(r1,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    rtd = Integer.parseInt(r2,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    deslcd = Integer.parseInt(deslc,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                break;
                case "001010": //INSTRUÇÃO DE TIPO SLTI
                    r1=instrucao.substring(6,11);
                    r2=instrucao.substring(11,16);
                    deslc=instrucao.substring(16,32);
                    rsd = Integer.parseInt(r1,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    rtd = Integer.parseInt(r2,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                    deslcd = Integer.parseInt(deslc,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                break;
                case "000010": //INSTRUÇÃO DE TIPO J
                    deslc = instrucao.substring(6,32);
                    deslcd = Integer.parseInt(deslc,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                break;
                case "000011": //INSTRUÇÃO DE TIPO JAL
                    deslc = instrucao.substring(6,32);
                    deslcd = Integer.parseInt(deslc,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                break;
                case "001111": //INSTRUÇÃO DE TIPO LUI
                break;
                default:
                 System.out.print("erro no opcode");
            }
        }
        return 0;
    }
    
    /*
    *
    *NESSA FUNÇÃO NOS MANDAMOS PARAMETROS PARA A ULA REALIZAR AS OPERAÇÃO DE
    *ACORDO COM SEUS OPCODE E CAMPOS FUNCT E GUARDANDO NO REGISTRADOR DESTINO
    *REALIZA A TERCEIRA PARTE DO CICLO
    */
    
    public int operandoula(){
       if(vazio==false){
            ULA u = new ULA();
           switch(instrucao.substring(0,6)) {
                case "000000": // INSTRUÇÃO DO TIPO R
                    if(funct.equals("100000")){ //SE OPERAÇÃO FOR ADD
                        resultula = u.operacao(regs[rdd],regs[rsd],regs[rtd],"000000","100000");
                    }
                    if(funct.equals("100010")){ // SE OPERAÇÃO FOR SUB
                         resultula = u.operacao(regs[rdd],regs[rsd],regs[rtd],"000000","100010");
                    }
                    if(funct.equals("100100")){ // SE OPERAÇÃO FOR AND
                         resultula = u.operacao(regs[rdd],regs[rsd],regs[rtd],"000000","100100");
                    }
                    if(funct.equals("100101")){ // SE OPERAÇÃO FOR OR
                         resultula = u.operacao(regs[rdd],regs[rsd],regs[rtd],"000000","100101");
                    }
                    if(funct.equals("100111")){ // SE OPERAÇÃO FOR NOR

                    }
                    if(funct.equals("101010")){ // SE OPERAÇÃO FOR SLT
                         resultula = u.operacao(regs[rdd],regs[rsd],regs[rtd],"000000","101010");
                    }
                    if(funct.equals("000000")){ // SE OPERAÇÃO FOR SLL
                        resultula = u.operacao(regs[rdd],regs[rsd],shamt,"000000","000000");
                    }
                    if(funct.equals("000010")){ // SE OPERAÇÃO FOR SRL
                         resultula = u.operacao(regs[rdd],regs[rsd],shamt,"000000","000010");
                    }
                    if(funct.equals("001000")){ // SE OPERAÇÃO FOR JR
                        regs[32]=regs[rdd];
                    }
                    if(funct.equals("011000")){ // SE OPERAÇÃO FOR JR
                        resultula = u.operacao(regs[rdd],regs[rsd],regs[rtd],"000000","011000");
                    }
                break;
                case "100011": //INSTRUÇÃO DE TIPO LW
                  deslcd = u.operacao(regs[rsd],regs[rtd],deslcd,"100011","000000");
                break;
                case "101011": //INSTRUÇÃO DE TIPO sw
                  deslcd = u.operacao(regs[rsd],regs[rtd],deslcd,"101011","000000");
                break;
                case "000100": // INSTRUÇÃO DO TIPO BEQ
                    if(u.operacao(regs[rsd],regs[rtd],deslcd,"000100","000000")==0){
                        regs[32]=deslcd;
                    }
                    
                break;
                case "000101": //INSTRUÇÃO DO TIPO BNE
                    if(u.operacao(regs[rsd],regs[rtd],deslcd,"000100","000000")!=0){
                        regs[32]=deslcd;
                    }
                break;
                case "001000": //INSTRUÇÃO DO TIPO ADDI
                     resultula = u.operacao(regs[rsd],regs[rtd],deslcd,"001000","000000");
                break;
                case "001010": //INSTRUÇÃO DO TIPO SLTI
                    resultula=u.operacao(regs[rsd],regs[rtd],deslcd,"001010","000000");
                break;
                case "001101": //INSTRUÇÃO DO TIPO ORI
                    resultula = u.operacao(regs[rsd],regs[rtd],deslcd,"000000","100101");
                break;
                case "000010": // INSTRUÇÃO DO TIPO J
                    resultula = u.operacao(0,regs[32],deslcd,"000000","100000");
                    regs[32] = resultula;
                break;
                case "000011": //INSTRUÇÃO DO TIPO JAL
                    regs[31]=regs[32]+4;
                    regs[32]=(deslcd/4)*4;
                break;
                case "001111": //INSTRUÇÃO DO TIPO LUI
                break;
                default:
                    System.out.print("erro na operação para ula");
            }
       }
        return 0;
    }
    
    /*
    *
    *4 ESCRITAS NOS REGISTRADORES 
    *
    */
    
       public void escreg(){  
        if(vazio==false){
           switch(instrucao.substring(0,6)) {
                case "000000": // INSTRUÇÃO DO TIPO R
                    if(funct.equals("001000")){ // SE OPERAÇÃO FOR JR
                        regs[32] = resultula;
                    }
                    else{
                    regs[rdd]=resultula;
                    }
                break;
                case "101011":
                    binario b = new binario();
                    int f=0;
                    String j;
                    j = b.binario(regs[rsd]);
                    f = 32 - j.length();

                    for(int z=0;z< f;z++){
                         j = "0"+j;
                    }
                    if(Integer.parseInt(String.valueOf((dados.getValueAt(deslcd,1))))!=1){
                        dados.setValueAt(1,deslcd,1);
                        dados.setValueAt(1,deslcd,2);
                        dados.setValueAt(j.substring(0,20),deslcd,3);
                        dados.setValueAt(j,deslcd,4);
                    }
                    else{
                        ins=String.valueOf(dados.getValueAt(deslcd,4));
                        model2.setValueAt(ins.substring(0,8),deslcd,0);
                        model2.setValueAt(ins.substring(8,16),deslcd,1);
                        model2.setValueAt(ins.substring(16,24),deslcd,2);
                        model2.setValueAt(ins.substring(24,32),deslcd,3);
                        
                        dados.setValueAt(1,deslcd,1);
                        dados.setValueAt(1,deslcd,2);
                        dados.setValueAt(j.substring(0,20),deslcd,3);
                        dados.setValueAt(j,deslcd,4);
                    }
                break;
                default:
                    regs[rsd]=resultula;
           }
           if(instrucao.substring(0,6).equals("100011")){ // SE FOR INSTRUÇÃO LW
               ins = String.valueOf(dados.getValueAt(deslcd,4));
               for(int y=0;y<1;y++){ //pagando um ciclo a mais para escrever no regs
                regs[rsd] = Integer.parseInt(ins,2); //CONVERTANDO DE BINARIO PARA INTEIRO
                regs[33]++;
               }
           }  
       }
    }
}
