package simulador;

/*
* ESSA FUNÇÃO SERVE PARA DECODIFICAR O QUE FOI DIGITADO PELO USUARIO
* E CARREGAR ESSES DADOS EM BINARIO PARA A MEMÓRIA PRINCIPAL 
*/

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

public class decodificador {
    
    /*
    *
    *DECLARAÇÃO DE VARIAVEIS 
    *
    */
    
    mips m = new mips();
    binario b = new binario();
    ArrayList op;
    String func = new String();
    public DefaultTableModel model;
    public DefaultTableModel model1,model2;
    String[] textoSeparado,textoSeparado2,t1,t2,t3;
    float tam_cache=0;
    public String deslocamento,instrucao,shamt;
    public int tipo=0,associatividade=1,flag=0,selectop=0;
    
    // vetor de opcodes de acordo *organizado de acordo com vetor de string
    String[] opcode = {"100011","101011","000100","000101","000000","001000","000000","000000","000000","001101","000000","000000","001010","000010","000000","000000","000000","000011","001111","000000"}; //19   
    //{"lw","sw","beq","bne","add","addi","sub","and","or","ori","nor","slt","slti","j","jr","sll","srl","jal","lui","mult"};

    // vetor com os campos funct 
    String[] funct = {"100000","100010","100101","100100","101010","000000","000010","001000","011000"};
    //valores dos registradores
    String[] t = {"1000","1001","1010","1011","1100","1101","1110","1111","11000","11001"}; 
    //valores dos registradores
    String[] s = {"10000","10001","10010","10011","10100","10101","10110","10111","11110"}; 
    
    public decodificador(DefaultTableModel model1,ArrayList op,float tam_cache,int associatividade) {
        this.tam_cache = tam_cache;
        this.associatividade = associatividade;
        this.model1 = model1;
        this.op = op;
    }
    
    
    public void inicache(){
        model = (DefaultTableModel) m.cache_dados.getModel();
        model2 = (DefaultTableModel) m.cache_instrucao.getModel();
        float capac_usuario=4,conjuntos=0;
        if(associatividade==3){
            associatividade=4;
        }
        else if(associatividade==4){
            associatividade=8;
        }
        
        else if(associatividade==5){
            associatividade=16;
        }
        m.associatividade=associatividade;
        capac_usuario = capac_usuario * associatividade;
        tam_cache = tam_cache * 1024;
        conjuntos = tam_cache / capac_usuario;
        m.tambloco=conjuntos;
        for(int i=0;i<(int)conjuntos;i++){
            for(int w=0;w<associatividade;w++){
                for(int z=0;z<4;z++){
                    model.addRow(new Object[]{i,0,"","",""});
                    model2.addRow(new Object[]{i,"","",""});
                }
            }
        }
    }
    
    /*
    *
    *FUNÇÃO QUE DECODIFICA EM TIPO R,I,J AS INSTRUÇÕES E ATRIBIU SEUS RESPECTIVOS
    *CAMPOS
    *
    */
    
    public void decodifica(){
        String texto="",r1 = null,r2 = null,r3 = null;
        int f=0;
    
        model = (DefaultTableModel) m.dram.getModel();
            for(int i=0;i<model1.getRowCount();i++){
                tipo=0;                
                //SEPARANDO OPERAÇÃO DO USUARIO
                texto = (String) model1.getValueAt(i,0);
                if(texto.equals("")){
                    model.addRow(new Object[]{""});
                }
                else{
                    textoSeparado = texto.split(" ");
                    func=textoSeparado[0];
                    //VERIFICANDO SE É UMA INSTRUÇÃO DO TIPO J
                    if(textoSeparado[0].equals("j") || textoSeparado[0].equals("jal")){
                        tipo=3;
                        textoSeparado[0].split("j");
                        deslocamento=b.binario(Integer.parseInt(textoSeparado[1]));
                    }
                    else{
                        //SEPARANDO PRIMEIRO REGISTRADO
                        if(textoSeparado[0].equals("jr")){
                            textoSeparado2 = textoSeparado[1].split(",");
                            if(textoSeparado2[0].contains("zero")){
                                r1=String.valueOf(0);
                            }
                            else if(textoSeparado2[0].contains("t")){ //Se for diferente de -1 é pq existe o caracter.
                                t1 = textoSeparado2[0].split("t");
                                r1=t[Integer.parseInt(t1[1])];
                            }
                            else{
                                if(textoSeparado2[0].contains("sp")){
                                    r1="11101";
                                }
                                else if(textoSeparado2[0].contains("ra")){
                                    r1="11111";
                                }
                                else{
                                    t1 = textoSeparado2[0].split("s");
                                    r1=s[Integer.parseInt(t1[1])];
                                }
                            }
                            tipo=1;
                        }
                        else{
                        textoSeparado2 = textoSeparado[1].split(",");
                            if(textoSeparado2[0].contains("zero")){
                                r1=String.valueOf(0);
                            }
                            else if(textoSeparado2[0].contains("t")){ //Se for diferente de -1 é pq existe o caracter.
                                t1 = textoSeparado2[0].split("t");
                                r1=t[Integer.parseInt(t1[1])];
                            }
                            else{
                                if(textoSeparado2[0].contains("sp")){
                                    r1="11101";
                                }
                                else if(textoSeparado2[0].contains("ra")){
                                    r1="11111";
                                }
                                else{
                                    t1 = textoSeparado2[0].split("s");
                                    r1=s[Integer.parseInt(t1[1])];
                                }
                            }
                        }
                            // SEPARANDO O SEGUNDO REGISTRADOR E VERIFICANDO SE É TIPO R OU I
                            if(textoSeparado2.length == 3 || textoSeparado2.length == 2 ){
                                if(textoSeparado2.length == 2){
                                    tipo=2;
                                    if(textoSeparado2[1].contains("zero")){
                                        r2=String.valueOf(0);
                                     }
                                    else if(textoSeparado2[1].contains("t")){
                                        textoSeparado = textoSeparado2[1].split("t");
                                        deslocamento=b.binario(Integer.parseInt(textoSeparado[0]));
                                        r2=t[Integer.parseInt(textoSeparado[1])];
                                    }
                                    else{
                                        if(textoSeparado2[1].contains("sp")){
                                              r2="11101";
                                              textoSeparado = textoSeparado2[1].split("sp");
                                              deslocamento=b.binario(Integer.parseInt(textoSeparado[0]));
                                        }
                                        else if(textoSeparado2[1].contains("ra")){
                                            r2="11111";
                                        }
                                     else{
                                        textoSeparado = textoSeparado2[1].split("s");
                                        deslocamento=b.binario(Integer.parseInt(textoSeparado[0]));
                                        r2=s[Integer.parseInt(textoSeparado[1])];
                                    }
                                    }
                                }
                                else{
                                    if(textoSeparado2[1].contains("zero")){
                                        r2=String.valueOf(0);
                                    }
                                    else if(textoSeparado2[1].contains("t")){ //Se for diferente de -1 é pq existe o caracter.
                                       t2 = textoSeparado2[1].split("t");
                                       r2=t[Integer.parseInt(t2[1])];
                                    }
                                    else{
                                        if(textoSeparado2[1].contains("sp")){
                                            r2="11101";
                                        }
                                        else if(textoSeparado2[1].contains("ra")){
                                            r2="11111";
                                        }
                                        else{
                                            t2 = textoSeparado2[1].split("s");
                                            r2=s[Integer.parseInt(t2[1])];
                                        }
                                    }
                                }
                            }

                            // SEPARANDO TERCEIRO REGISTRADOR 
                            if(textoSeparado2.length == 3 ){
                                tipo=1;
                                flag=0; //PARA VERIFICAR É EXISTE UM REGISTRADOR OU UM NUMERO INTEIRO
                                if(textoSeparado2[2].contains("zero")){
                                    r3=String.valueOf(0);
                                    f =0;
                                    f = 5 - r3.length();
                                    for(int z=0;z< f;z++){
                                       r3 = "0"+r3;
                                    }
                                    flag=1;
                                }
                                if(textoSeparado2[2].contains("sp")){
                                    r3="11101";
                                    flag=1;
                                }
                                if(textoSeparado2[2].contains("ra")){
                                    r3="11111";
                                }
                                if(textoSeparado2[2].contains("t")){ //Se for diferente de -1 é pq existe o caracter.
                                    t3 = textoSeparado2[2].split("t");
                                    r3=t[Integer.parseInt(t3[1])];
                                    f =0;
                                    f = 5 - r3.length();
                                    for(int z=0;z< f;z++){
                                       r3 = "0"+r3;
                                    }
                                    flag=1;
                                }
                                if(textoSeparado2[2].contains("s")){
                                    t3 = textoSeparado2[2].split("s");
                                    r3=s[Integer.parseInt(t3[1])];
                                    f =0;
                                    f = 5 - r3.length();
                                    for(int z=0;z< f;z++){
                                        r3 = "0"+r3;
                                    }
                                    flag=1;
                                }
                                //SE FOR UM NUMERO INTEIRO É DIVIDIDO EM CAMPO SHAMT OU DESLOCAMENTO/IMEDIATO
                                if(flag==0){
                                     if(opcode[(Integer)op.get(selectop)].equalsIgnoreCase("000000")){
                                        tipo=1;
                                        shamt=b.binario(Integer.parseInt(textoSeparado2[2]));
                                    }
                                    else{
                                        tipo=2;
                                        deslocamento=b.binario(Integer.parseInt(textoSeparado2[2]));
                                    }
                                }

                        }
                        else{
                            r3="000000";
                        }

                        //EXTENDENDO O NUMERO 
                        f = 5 - r1.length();
                        for(int z=0;z< f;z++){
                            r1 = "0"+r1;
                        }
                        f =0;
                        f = 5 - r2.length();
                        for(int z=0;z< f;z++){
                            r2 = "0"+r2;
                        }
                    }
                        
                        /*
                        *VERIFICANDO SE OP É 000000 E VENDO QUAL SEU RESPECTIVO
                        * CAMPO FUNCT PARA MANDAR PARA ULA
                        */
                         if(opcode[(Integer)op.get(selectop)].equalsIgnoreCase("000000")){
                            if(func.equalsIgnoreCase("add")){
                                func=funct[0];
                            }
                            if(func.equalsIgnoreCase("sub")){
                                func=funct[1];
                            }
                            if(func.equalsIgnoreCase("or")){
                                func=funct[2];
                            }
                            if(func.equalsIgnoreCase("and")){
                                func=funct[3];
                            }
                            if(func.equalsIgnoreCase("slt")){
                                func=funct[4];
                            }
                            if(func.equalsIgnoreCase("sll")){
                                func=funct[5];
                            }
                            if(func.equalsIgnoreCase("srl")){
                                func=funct[6];
                            }
                            if(func.equalsIgnoreCase("jr")){
                                func=funct[7];
                            }
                            if(func.equalsIgnoreCase("mult")){
                                func=funct[8];
                            }
                        }
                        else {
                            func="000000";
                        }

                        // SEPARAÇÃO DO CAMPO SHAMT PARA DESLOCAMENTOS 

                        if(!func.equals(funct[6]) && !func.equals(funct[5])){
                            shamt="00000";
                        }

                         /*
                         *CRIANDO INSTRUÇÃO COM TODOS PARAMETROS FORNECIDOS
                         * E ADICIONANDO NA MEMORIA PRINCIPAL
                         */

                         /*
                         *SE INSTRUÇÃO FOR DO TIPO R
                         */
                         if(tipo == 1){ 
                             if(func.equals(funct[5]) || func.equals(funct[6])){
                                f=0;
                                f = 5 - shamt.length();
                                for(int z=0;z< f;z++){
                                   shamt= "0"+shamt;
                                }
                                instrucao =opcode[(Integer)op.get(selectop)]+r1+r2+"00000"+shamt+func;
                            }
                             else if(func.equals(funct[7])){
                                 instrucao =opcode[(Integer)op.get(selectop)]+r1+"00000"+"00000"+shamt+func;
                             }
                            else{
                                instrucao =opcode[(Integer)op.get(selectop)]+r1+r2+r3+shamt+func;  
                            }
                            System.out.println(instrucao);
                         }

                         /*
                         *SE INSTRUÇÃO FOR DO TIPO I
                         */

                         if(tipo == 2){  
                            f=0;
                                f = 16 - deslocamento.length();
                                for(int z=0;z< f;z++){
                                    deslocamento = "0"+deslocamento;
                                }
                                instrucao =opcode[(Integer)op.get(selectop)]+r1+r2+deslocamento;
                            System.out.println(instrucao);
                         }

                         /*
                         *SE INSTRUÇÃO FOR DO TIPO J
                         */

                         if(tipo == 3){
                             f=0;
                             f = 26 - deslocamento.length();
                             for(int z=0;z< f;z++){
                                deslocamento = "0"+deslocamento;
                            }
                            instrucao =opcode[(Integer)op.get(selectop)]+deslocamento;
                            System.out.println(instrucao);
                         }                    
                        model.addRow(new Object[]{instrucao.substring(0,8),instrucao.substring(8,16),instrucao.substring(16,24),instrucao.substring(24,32)});
                selectop++;
                }
            }
            m.setVisible(true);
    }
}
