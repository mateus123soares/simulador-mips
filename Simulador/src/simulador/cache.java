/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulador;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class cache {
    int pc1,flag;
    float tambloco,numbloco;
    int[] vet;
    String tag;
    DefaultTableModel modelinstrucao,modeldados,model1,modelfalhas;

    public cache(int pc,String tag,DefaultTableModel cacheinstrucao,DefaultTableModel cachedados,DefaultTableModel model,int[] vet,DefaultTableModel modelfalhas,float tambloco) {
        this.pc1 = pc;
        this.tag = tag;
        this.modeldados = cachedados;
        this.modelinstrucao = cacheinstrucao;
        this.model1 = model;
        this.vet = vet;
        this.tambloco=tambloco;
        this.modelfalhas = modelfalhas;
    }
    
    public int cachediretamente(){
        if(!(tag.equals("-")) && !(tag.equals(""))){   //VERIFICANDO SE A INFORMAÇÃO CHEGA VAZIA
            tag = tag + String.valueOf(model1.getValueAt(pc1,1));
            tag = tag + String.valueOf(model1.getValueAt(pc1,2));
            tag = tag.substring(0,20); //PEGANDO OS 20BITS PARA TAG
            for(int i=0;i<modelinstrucao.getRowCount();i++){ //VERIFICANDO TODA A CACHE EM BUSCA DE TAG
                if(tag.equals(modelinstrucao.getValueAt(i,2))){ //SE ACHAR A TAG ELE FAZ AS OPERAÇÃO DA INSTRUCAO
                    flag=1;
                        controle c = new controle(modelinstrucao,vet[32]/4,vet,modeldados,model1);
                        for(int y=0;y<1;y++){
                            c.busca_instrucao();
                        }
                        vet[33]++;
                        for(int y=0;y<1;y++){
                            c.leituradeoperando();
                        }
                        vet[33]++;
                        for(int y=0;y<1;y++){
                            c.operandoula();
                        }
                        vet[33]++;
                        if(pc1 == vet[32]/4){
                            c.escreg();
                            vet[33]++;
                        vet[32]=vet[32]+4;
                        }
                        
            }
            }
        if(flag==0){ //SE ELE NÃO ACHAR ELE BUSCA A INSTRUÇÃO
            modelfalhas.addRow(new Object[]{"Instrucao " + vet[32]/4});
            for(int i=0;i<50;i++){ //PAGA OS 50 CICLOS
              vet[33]++;
            }
            for(int i=0;i<4;i++){ //TRAS AS OUTRAS 3 INSTRUÇÕES ADIJACENTES 
                String tag1;
                int numerobloco=(vet[32]/4)+i;
                tag1 = String.valueOf(model1.getValueAt(numerobloco,0));
                    if(!(tag1.equals("-")) && !(tag1.equals(""))){
                        tag1 = tag1 + String.valueOf(model1.getValueAt(numerobloco,1));
                        tag1 = tag1 + String.valueOf(model1.getValueAt(numerobloco,2));
                        tag1 = tag1.substring(0,20);
                        modelinstrucao.setValueAt(1,numerobloco,1);
                        modelinstrucao.setValueAt(tag1,numerobloco,2);
                        String instrucao;
                        instrucao = String.valueOf(model1.getValueAt((vet[32]/4)+i,0));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,1));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,2));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,3));
                        modelinstrucao.setValueAt(instrucao,numerobloco,3);
                    }
            }
        }
        }
        else{
            if(tag.equals("-")){
                flag=1;
            }
            else{
                flag=2;
                vet[32]=vet[32]+4;
            }
        }
        return flag;
    }
    
public int cache4vias(){
        
        if(!(tag.equals("-")) && !(tag.equals(""))){   //VERIFICANDO SE A INFORMAÇÃO CHEGA VAZIA
            tag = tag + String.valueOf(model1.getValueAt(pc1,1));
            tag = tag + String.valueOf(model1.getValueAt(pc1,2));
            tag = tag.substring(0,20); //PEGANDO OS 20BITS PARA TAG
            for(int i=0;i<modelinstrucao.getRowCount();i++){ //VERIFICANDO TODA A CACHE EM BUSCA DE TAG
                if(tag.equals(modelinstrucao.getValueAt(i,2))){ //SE ACHAR A TAG ELE FAZ AS OPERAÇÃO DA INSTRUCAO
                    flag=1;
                    if(((vet[32]/4)%4)==0 && vet[32]!=0){
                        vet[34]=((vet[32]/4)/4)*16;
                    }
                        controle c = new controle(modelinstrucao,vet[34],vet,modeldados,model1);
                        for(int y=0;y<1;y++){
                            c.busca_instrucao();
                        }
                        vet[33]++;
                        for(int y=0;y<1;y++){
                            c.leituradeoperando();
                        }
                        vet[33]++;
                        for(int y=0;y<1;y++){
                            c.operandoula();
                        }
                        vet[33]++;
                        if(pc1 == vet[32]/4){
                            c.escreg();
                            vet[33]++;
                        vet[32]=vet[32]+4;
                        }
                        vet[34]++;
            }
            }
        if(flag==0){ //SE ELE NÃO ACHAR ELE BUSCA A INSTRUÇÃO
            modelfalhas.addRow(new Object[]{"Instrucao " + vet[32]/4});
            for(int i=0;i<50;i++){ //PAGA OS 50 CICLOS
              vet[33]++;
            }
            int flag1=0;
            numbloco=vet[32]/16;
            double numbloco1=numbloco%tambloco;
            numbloco=16*(int)numbloco1;
            if(!(modelinstrucao.getValueAt((int)numbloco,1).equals(""))){
                    System.out.println("caiu aqdsa");
                    flag1=1;
            }
                
            for(int i=0;i<4;i++){ //TRAS AS OUTRAS 3 INSTRUÇÕES ADIJACENTES 
                String tag1;
                int numerobloco=(vet[32]/4);
                numbloco=vet[32]/16;
                numbloco1=numbloco%tambloco;
                numbloco=16*(int)numbloco1;
                
                if(flag1==1){
                    numbloco=numbloco+4;        
                }
                numbloco=numbloco+i;
                numerobloco=numerobloco+i;
                
                tag1 = String.valueOf(model1.getValueAt(numerobloco,0));
                    if(!(tag1.equals("-")) && !(tag1.equals(""))){
                        tag1 = tag1 + String.valueOf(model1.getValueAt(numerobloco,1));
                        tag1 = tag1 + String.valueOf(model1.getValueAt(numerobloco,2));
                        tag1 = tag1.substring(0,20);
                        
                        modelinstrucao.setValueAt(1,(int)numbloco,1);
                        modelinstrucao.setValueAt(tag1,(int)numbloco,2);
                        String instrucao;
                        instrucao = String.valueOf(model1.getValueAt((vet[32]/4)+i,0));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,1));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,2));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,3));
                        modelinstrucao.setValueAt(instrucao,(int)numbloco,3);
                    }
            }
            flag1=0;
        }
        }
        else{
            if(tag.equals("-")){
                flag=1;
            }
            else{
                flag=2;
                vet[34]++;
                vet[32]=vet[32]+4;
            }
        }
        return flag;
}
public int cache8vias(){
        
        if(!(tag.equals("-")) && !(tag.equals(""))){   //VERIFICANDO SE A INFORMAÇÃO CHEGA VAZIA
            tag = tag + String.valueOf(model1.getValueAt(pc1,1));
            tag = tag + String.valueOf(model1.getValueAt(pc1,2));
            tag = tag.substring(0,20); //PEGANDO OS 20BITS PARA TAG
            for(int i=0;i<modelinstrucao.getRowCount();i++){ //VERIFICANDO TODA A CACHE EM BUSCA DE TAG
                if(tag.equals(modelinstrucao.getValueAt(i,2))){ //SE ACHAR A TAG ELE FAZ AS OPERAÇÃO DA INSTRUCAO
                    flag=1;
                    if(((vet[32]/4)%4)==0 && vet[32]!=0){
                        vet[34]=((vet[32]/4)/4)*32;
                        
                    }
                        controle c = new controle(modelinstrucao,vet[34],vet,modeldados,model1);
                        for(int y=0;y<1;y++){
                            c.busca_instrucao();
                        }
                        vet[33]++;
                        for(int y=0;y<1;y++){
                            c.leituradeoperando();
                        }
                        vet[33]++;
                        for(int y=0;y<1;y++){
                            c.operandoula();
                        }
                        vet[33]++;
                        if(pc1 == vet[32]/4){
                            c.escreg();
                            vet[33]++;
                        vet[32]=vet[32]+4;
                        }
                        vet[34]++;
            }
            }
        if(flag==0){ //SE ELE NÃO ACHAR ELE BUSCA A INSTRUÇÃO
            modelfalhas.addRow(new Object[]{"Instrucao " + vet[32]/4});
            for(int i=0;i<50;i++){ //PAGA OS 50 CICLOS
              vet[33]++;
            }
            int flag1=0;
            numbloco=vet[32]/16;
            double numbloco1=numbloco%tambloco;
            numbloco=32*(int)numbloco1;
            if(!(modelinstrucao.getValueAt((int)numbloco,1).equals(""))){
                    System.out.println("caiu aqdsa");
                    flag1=1;
            }
             
            for(int i=0;i<4;i++){ //TRAS AS OUTRAS 3 INSTRUÇÕES ADIJACENTES 
                String tag1;
                int numerobloco=(vet[32]/4);
                numbloco=vet[32]/16;
                numbloco1=numbloco%tambloco;
                numbloco=32*(int)numbloco1;
                
                numbloco=numbloco+i;
                numerobloco=numerobloco+i;
                tag1 = String.valueOf(model1.getValueAt(numerobloco,0));
                    if(!(tag1.equals("-")) && !(tag1.equals(""))){
                        tag1 = tag1 + String.valueOf(model1.getValueAt(numerobloco,1));
                        tag1 = tag1 + String.valueOf(model1.getValueAt(numerobloco,2));
                        tag1 = tag1.substring(0,20);
                        modelinstrucao.setValueAt(1,(int)numbloco,1);
                        modelinstrucao.setValueAt(tag1,(int)numbloco,2);
                        String instrucao;
                        instrucao = String.valueOf(model1.getValueAt((vet[32]/4)+i,0));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,1));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,2));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,3));
                        modelinstrucao.setValueAt(instrucao,(int)numbloco,3);
                    }
            }
            flag1=0;
        }
        }
        else{
            if(tag.equals("-")){
                flag=1;
            }
            else{
                flag=2;
                vet[34]++;
                vet[32]=vet[32]+4;
            }
        }
        return flag;
}
public int cache16vias(){
        
        if(!(tag.equals("-")) && !(tag.equals(""))){   //VERIFICANDO SE A INFORMAÇÃO CHEGA VAZIA
            tag = tag + String.valueOf(model1.getValueAt(pc1,1));
            tag = tag + String.valueOf(model1.getValueAt(pc1,2));
            tag = tag.substring(0,20); //PEGANDO OS 20BITS PARA TAG
            for(int i=0;i<modelinstrucao.getRowCount();i++){ //VERIFICANDO TODA A CACHE EM BUSCA DE TAG
                if(tag.equals(modelinstrucao.getValueAt(i,2))){ //SE ACHAR A TAG ELE FAZ AS OPERAÇÃO DA INSTRUCAO
                    flag=1;
                    if(((vet[32]/4)%4)==0 && vet[32]!=0){
                        vet[34]=((vet[32]/4)/4)*64;
                        
                    }
                        controle c = new controle(modelinstrucao,vet[34],vet,modeldados,model1);
                        for(int y=0;y<1;y++){
                            c.busca_instrucao();
                        }
                        vet[33]++;
                        for(int y=0;y<1;y++){
                            c.leituradeoperando();
                        }
                        vet[33]++;
                        for(int y=0;y<1;y++){
                            c.operandoula();
                        }
                        vet[33]++;
                        if(pc1 == vet[32]/4){
                            c.escreg();
                            vet[33]++;
                        vet[32]=vet[32]+4;
                        }
                        vet[34]++;
            }
            }
        if(flag==0){ //SE ELE NÃO ACHAR ELE BUSCA A INSTRUÇÃO
            modelfalhas.addRow(new Object[]{"Instrucao " + vet[32]/4});
            for(int i=0;i<50;i++){ //PAGA OS 50 CICLOS
              vet[33]++;
            }
            int flag1=0;
            numbloco=vet[32]/16;
            double numbloco1=numbloco%tambloco;
            numbloco=64*(int)numbloco1;
            if(!(modelinstrucao.getValueAt((int)numbloco,1).equals(""))){
                    flag1=1;
            }
             
            for(int i=0;i<4;i++){ //TRAS AS OUTRAS 3 INSTRUÇÕES ADIJACENTES 
                String tag1;
                int numerobloco=(vet[32]/4);
                numbloco=vet[32]/16;
                numbloco1=numbloco%tambloco;
                numbloco=64*(int)numbloco1;
                if(modelinstrucao.getValueAt((int)numbloco,2).equals("")){
                    
                }
                else{
                    numbloco=numbloco+4;
                }
                
            numbloco=numbloco+i;
            numerobloco=numerobloco+i;
                tag1 = String.valueOf(model1.getValueAt(numerobloco,0));
                    if(!(tag1.equals("-")) && !(tag1.equals(""))){
                        tag1 = tag1 + String.valueOf(model1.getValueAt(numerobloco,1));
                        tag1 = tag1 + String.valueOf(model1.getValueAt(numerobloco,2));
                        tag1 = tag1.substring(0,20);
                        modelinstrucao.setValueAt(1,(int)numbloco,1);
                        modelinstrucao.setValueAt(tag1,(int)numbloco,2);
                        String instrucao;
                        instrucao = String.valueOf(model1.getValueAt((vet[32]/4)+i,0));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,1));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,2));
                        instrucao = String.valueOf(instrucao + model1.getValueAt(numerobloco,3));
                        modelinstrucao.setValueAt(instrucao,(int)numbloco,3);
                    }
            }
            flag1=0;
        }
        }
        else{
            if(tag.equals("-")){
                flag=1;
            }
            else{
                flag=2;
                vet[34]++;
                vet[32]=vet[32]+4;
            }
        }
        return flag;
}
}
