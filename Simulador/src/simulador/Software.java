package simulador;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Software extends javax.swing.JFrame{
    public Software() {
        initComponents();
    }
    int i=0;
    
    //vetor de string para saber qual operação sera realizada e se é valida
    
    ArrayList op = new ArrayList();
    String codigo[] = {"lw","sw","beq","bne","add","addi","sub","and","or","ori","nor","slt","slti","j","jr","sll","srl","jal","lui","mult","",};
    
    public String[] textoSeparado2;
    public DefaultTableModel model1;
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        codigoFonte = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        bt_addCod = new javax.swing.JButton();
        bt_limpar = new javax.swing.JButton();
        bt_executa = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jcodigo = new javax.swing.JTable();
        bt_addCod1 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        canvas1 = new java.awt.Canvas();
        canvas2 = new java.awt.Canvas();
        label1 = new java.awt.Label();
        label2 = new java.awt.Label();
        t_tamanhocache = new java.awt.TextField();
        label3 = new java.awt.Label();
        c_associatividade = new java.awt.Choice();
        label4 = new java.awt.Label();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        codigoFonte.setText("Insira o Codigo aqui");
        codigoFonte.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                codigoFonteFocusGained(evt);
            }
        });
        codigoFonte.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                codigoFonteMouseClicked(evt);
            }
        });
        codigoFonte.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                codigoFonteKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                codigoFonteKeyReleased(evt);
            }
        });

        jLabel1.setText("Digite");

        bt_addCod.setText("Adicionar");
        bt_addCod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_addCodActionPerformed(evt);
            }
        });

        bt_limpar.setText("Limpar");
        bt_limpar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bt_limparMouseClicked(evt);
            }
        });
        bt_limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_limparActionPerformed(evt);
            }
        });

        bt_executa.setText("Executar");
        bt_executa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bt_executaMouseClicked(evt);
            }
        });
        bt_executa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_executaActionPerformed(evt);
            }
        });

        jcodigo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Instruções"
            }
        ));
        jScrollPane2.setViewportView(jcodigo);

        bt_addCod1.setText("Adicionar Linha");
        bt_addCod1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bt_addCod1MouseClicked(evt);
            }
        });
        bt_addCod1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_addCod1ActionPerformed(evt);
            }
        });

        label1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        label1.setText("CONFIGURAÇÕES DA CACHE");

        label2.setText("Informe o tamanho da cache");

        label3.setText("Selecione a associatividade da cache");

        c_associatividade.addItem("Diretamente mapeada");
        c_associatividade.addItem("Totalmente associativa");
        c_associatividade.addItem("4 Vias");
        c_associatividade.addItem("8 Vias");
        c_associatividade.addItem("16 Vias");

        label4.setText("KB");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(canvas2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(152, 152, 152))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(2, 2, 2)
                                        .addComponent(c_associatividade, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(bt_executa, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(75, 75, 75)))
                                .addComponent(canvas1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(t_tamanhocache, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(53, 53, 53)
                                .addComponent(bt_addCod, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(61, 61, 61)
                                .addComponent(bt_addCod1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(65, 65, 65)
                                .addComponent(bt_limpar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(codigoFonte, javax.swing.GroupLayout.PREFERRED_SIZE, 525, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codigoFonte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt_limpar)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bt_addCod)
                        .addComponent(bt_addCod1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(t_tamanhocache, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(23, 23, 23)
                                        .addComponent(canvas1, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(20, 20, 20)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(c_associatividade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(39, 39, 39)
                                        .addComponent(bt_executa, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(canvas2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(63, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void bt_addCodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_addCodActionPerformed
        i=0; // zerando variavel do while
        model1 = (DefaultTableModel) jcodigo.getModel();
        String[] textoSeparado = codigoFonte.getText().split(" ");   //Separar qual operação sera realizada
        
            while(i<20 && !(codigo[i].equals(textoSeparado[0])) ){ //Pesquisando se codigo existe
                i++;
            }
        if(i==20 || textoSeparado[1].isEmpty()){ //Mensagem de erro de sintaxe
            JOptionPane.showMessageDialog(null ,"Erro Sintaxe desconhecida");            
        }
        else{ //Se estiver tudo certo copia codigo para Area de texto
        String reg = textoSeparado[1];
        textoSeparado2 = reg.split(","); // Separando quais os registradores serão utilizado
                    op.add(i);
                    model1.addRow(new Object[]{textoSeparado[0] + " " + textoSeparado[1],0});
            }
    }//GEN-LAST:event_bt_addCodActionPerformed
    private void bt_limparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_limparActionPerformed

    }//GEN-LAST:event_bt_limparActionPerformed
    private void codigoFonteFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_codigoFonteFocusGained
       codigoFonte.setText("");
    }//GEN-LAST:event_codigoFonteFocusGained
    private void codigoFonteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_codigoFonteMouseClicked
 
    }//GEN-LAST:event_codigoFonteMouseClicked
    private void codigoFonteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_codigoFonteKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            model1 = (DefaultTableModel) jcodigo.getModel();
            i=0; // zerando variavel do while
            String[] textoSeparado = codigoFonte.getText().split(" ");   //Separar qual operação sera realizada
            while(i<20 && !(codigo[i].equals(textoSeparado[0])) ){ //Pesquisando se codigo existe
                i++;
            }
        if(i==20 || textoSeparado[1].isEmpty()){ //Mensagem de erro de sintaxe
            JOptionPane.showMessageDialog(null ,"Erro Sintaxe desconhecida");            
        }
        else{ //Se estiver tudo certo copia codigo para Area de texto
            String reg = textoSeparado[1];
            textoSeparado2 = reg.split(","); // Separando quais os registradores serão utilizado
                       op.add(i);
                       model1.addRow(new Object[]{textoSeparado[0] + " " + textoSeparado[1],0});
            }
        }
    }//GEN-LAST:event_codigoFonteKeyPressed

    private void codigoFonteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_codigoFonteKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_codigoFonteKeyReleased

    private void bt_executaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_executaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bt_executaActionPerformed

    private void bt_executaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bt_executaMouseClicked
        decodificador d = new decodificador(model1,op,Float.valueOf(t_tamanhocache.getText()),c_associatividade.getSelectedIndex()+1);
        d.inicache();
        d.decodifica();
    }//GEN-LAST:event_bt_executaMouseClicked

    private void bt_limparMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bt_limparMouseClicked
       model1.removeRow(model1.getRowCount()-1);
       op.remove(op.size());
    }//GEN-LAST:event_bt_limparMouseClicked

    private void bt_addCod1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_addCod1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bt_addCod1ActionPerformed

    private void bt_addCod1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bt_addCod1MouseClicked
      model1 = (DefaultTableModel) jcodigo.getModel();
        model1.addRow(new Object[]{""});
    }//GEN-LAST:event_bt_addCod1MouseClicked
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Software().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bt_addCod;
    private javax.swing.JButton bt_addCod1;
    private javax.swing.JButton bt_executa;
    private javax.swing.JButton bt_limpar;
    private java.awt.Choice c_associatividade;
    private java.awt.Canvas canvas1;
    private java.awt.Canvas canvas2;
    private javax.swing.JTextField codigoFonte;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jcodigo;
    private java.awt.Label label1;
    private java.awt.Label label2;
    private java.awt.Label label3;
    private java.awt.Label label4;
    private java.awt.TextField t_tamanhocache;
    // End of variables declaration//GEN-END:variables
}
